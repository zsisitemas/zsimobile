class CreateCupomitems < ActiveRecord::Migration[6.0]
  def change
    create_table :cupomitems do |t|
      t.float :valor
      t.float :total
      t.integer :quantidade
      t.integer :item
      t.string :status
      t.timestamps
    end
  end
end
