class AddVariosColumnsProdutos < ActiveRecord::Migration[6.0]
  def change
   #add_column :produtos, :cod_produto, :string
   #add_column :produtos, :descricao, :string
    add_column :produtos, :status, :string
    add_column :produtos, :categoria, :string
    add_column :produtos, :representante, :string
    add_column :produtos, :fone_representante, :string
    add_column :produtos, :estoque, :float
    add_column :produtos, :estoque_Mini, :float
    add_column :produtos, :estoque_Max, :float
    add_column :produtos, :preco_compra, :float
    add_column :produtos, :preco_custo,:float
    add_column :produtos, :preco_venda, :float
    add_column :produtos, :validade, :date
    add_column :produtos, :sub_categoria, :string
    add_column :produtos, :unidade, :string
  end
end
