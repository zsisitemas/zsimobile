class CreateCupoms < ActiveRecord::Migration[6.0]
  def change
    create_table :cupoms do |t|
      t.date :data
      t.string :forma_pagamento
      t.float :subtotal
      t.string :status
      t.string :caixa
      t.string :fechamento
      t.float :desconto
      t.string :operador
      t.time :hora

      t.timestamps
    end
  end
end
