class AddNomeToProduto < ActiveRecord::Migration[6.0]
  def change
    add_column :produtos, :nome, :string
    add_column :produtos, :imagem, :string
  end
end
