# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_03_28_161741) do

  create_table "cupomitems", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.float "valor"
    t.float "total"
    t.integer "quantidade"
    t.integer "item"
    t.string "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "cupoms", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.date "data"
    t.string "forma_pagamento"
    t.float "subtotal"
    t.string "status"
    t.string "caixa"
    t.string "fechamento"
    t.float "desconto"
    t.string "operador"
    t.time "hora"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "produtos", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "descricao"
    t.decimal "valor", precision: 10
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "status"
    t.string "categoria"
    t.string "representante"
    t.string "fone_representante"
    t.float "estoque"
    t.float "estoque_Mini"
    t.float "estoque_Max"
    t.float "preco_compra"
    t.float "preco_custo"
    t.float "preco_venda"
    t.date "validade"
    t.string "sub_categoria"
    t.string "unidade"
    t.string "nome"
    t.string "imagem"
  end

end
