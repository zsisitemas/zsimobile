# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


10.times do |i|
    produto_nome = Faker::Food.unique.dish
    produto_descricao = Faker::Food.unique.description
    produto_preco_venda = Faker::Commerce.unique.price

    produto = "#{produto_nome} - #{produto_descricao} - #{produto_preco_venda}"
    puts "Cadastrando o #{produto}"
    Produto.create(nome: produto_nome, descricao: produto_descricao, preco_venda: produto_preco_venda)
end