module Api
	module V1
		class CupomitemsController < ApplicationController
           
			def index
				cupomitem = Cupomitem.order('created_at DESC');
				render json: {status:'SUCCESS', message:'Cupomitems Carregados carregados',
				data:cupomitem},status: :ok
			end
           
		    def show
				cupomitem = Cupomitem.find(params[:id]);
				render json: {status: 'SUCCESS', menssage:'Cupomitem carregados',
				data:cupomitem},status: :ok			
			end
		
			def create
				cupomitem = Cupomitem.new(cupomitem_params)
					if cupomitem.save
						render json: {status:'SUCCES', message:'Cupomitem salvo', data:cupomitem},status: :ok
					else
						render json:{status:'ERROR',message:'Cupomitem não salvo',
						data:cupomitem.erros},status: :unprocessable_entity
					end
			end
        
			def  update
				cupomitem = Cupomitem.find(params[:id])
					if cupomitem.update_attributes(cupomitem_params)
						render json: {status: 'SUCCESS',message:'Cupomitem atualizado',data:cupomitem},status: :ok
					else
						render json: {status: 'ERROR', message:'Cupomitem não atualizado',
						data:cupomitem.erros},status: :unprocessable_entity
					end
			end
          
			def destroy
				cupomitem = Cupomitem.find(params[:id])
				cupomitem.destroy
				render json: {status: 'SUCCESS', menssage:'Cupomitem deletado', data:cupomitem},status: :ok
			end

			private

			def cupomitem_params
				params.require(:cupomitem).permit(:valor, :total, :quantidade, :item, :status)
			end
		end
	end
end