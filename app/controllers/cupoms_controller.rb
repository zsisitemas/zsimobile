module Api
	module V1
		class CupomsController < ApplicationController
			#before_action :set_produto, only: [:show, :edit, :update, :destroy]

           
			def index
				cupom = Cupom.order('created_at DESC');
				render json: {status:'SUCCESS', message:'Cupoms Carregados carregados',
				data:cupom},status: :ok
			end
           
		    def show
				cupom = Cupom.find(params[:id]);
				render json: {status: 'SUCCESS', menssage:'Cupom carregados',
				data:cupom},status: :ok			
			end
		
			def create
				cupom = Cupom.new(cupom_params)
					if cupom.save
						render json: {status:'SUCCES', message:'Cupom salvo', data:cupom},status: :ok
					else
						render json:{status:'ERROR',message:'Cupom não salvo',
						data:cupom.erros},status: :unprocessable_entity
					end
			end
        
			def  update
				cupom = Cupom.find(params[:id])
					if cupom.update_attributes(cupom_params)
						render json: {status: 'SUCCESS',message:'Cupom atualizado',data:cupom},status: :ok
					else
						render json: {status: 'ERROR', message:'Cupom não atualizado',
						data:cupom.erros},status: :unprocessable_entity
					end
			end
          
			def destroy
				cupom = Cupom.find(params[:id])
				cupom.destroy
				render json: {status: 'SUCCESS', menssage:'Cupom deletado', data:cupom},status: :ok
			end

			private

			def cupom_params
				params.require(:cupom).permit(:data, :forma_pagamento, :subtotal, :status, :caixa, :fechamento, :desconto, :operador,
				:hora)
			end

		end
	end
end