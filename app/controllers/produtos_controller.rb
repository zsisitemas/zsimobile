module Api
	module V1
		class ProdutosController < ApplicationController

			def index
				produto = Produto.order('created_at DESC');
				render json: {status:'SUCCESS', message:'Produtos Carregados carregados',
				data:produto},status: :ok
			end

			def show
				produto = Produto.find(params[:id]);
				render json: {status: 'SUCCESS', menssage:'Produto carregados',
				data:produto},status: :ok
				#render json: {produto}
			end
			
			def create
				produto = Produto.new(produto_params)
					if produto.save
						render json: {status:'SUCCESS', message:'Produto salvo', data:produto},status: :ok
					else
						render json:{status:'ERROR',message:'Produto não salvo',
						data:produto.erros},status: :unprocessable_entity
					end
			end

			def  update
				produto = Produto.find(params[:id])
					if produto.update_attributes(produto_params)
						render json: {status: 'SUCCESS',message:'Produto atualizado',data:produto},status: :ok
					else
						render json: {status: 'ERROR', message:'Produto não atualizado',
						data:produto.erros},status: :unprocessable_entity
					end
			end
					
			def destroy
				produto = Produto.find(params[:id])
				produto.destroy
				render json: {status: 'SUCCESS', menssage:'Produto deletado', data:produto},status: :ok
			end

			private
			
			def produto_params
				params.require(:produto).permit(:descricao, :valor, :status, :categoria, :representante, :fone_representante,
					:estoque, :estoque_Mini, :estoque_Max, :preco_compra, :preco_custo, :preco_venda, :validade, :sub_categoria,
					:unidade, :nome, :imagem)
			end
		end
	end
end
